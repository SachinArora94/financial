(function() {
    'use strict';

    angular
        .module('jhipsterNewApp')
        .controller('MyEntityDetailController', MyEntityDetailController);

    MyEntityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MyEntity'];

    function MyEntityDetailController($scope, $rootScope, $stateParams, previousState, entity, MyEntity) {
        var vm = this;

        vm.myEntity = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jhipsterNewApp:myEntityUpdate', function(event, result) {
            vm.myEntity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
