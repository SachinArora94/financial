package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.MyEntity;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the MyEntity entity.
 */
@Repository
public class MyEntityRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<MyEntity> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public MyEntityRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(MyEntity.class);
        this.findAllStmt = session.prepare("SELECT * FROM myEntity");
        this.truncateStmt = session.prepare("TRUNCATE myEntity");
    }

    public List<MyEntity> findAll() {
        List<MyEntity> myEntitiesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                MyEntity myEntity = new MyEntity();
                myEntity.setId(row.getUUID("id"));
                myEntity.setName(row.getString("name"));
                myEntity.setPhone(row.getInt("phone"));
                myEntity.setEmail(row.getString("email"));
                return myEntity;
            }
        ).forEach(myEntitiesList::add);
        return myEntitiesList;
    }

    public MyEntity findOne(UUID id) {
        return mapper.get(id);
    }

    public MyEntity save(MyEntity myEntity) {
        if (myEntity.getId() == null) {
            myEntity.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<MyEntity>> violations = validator.validate(myEntity);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(myEntity);
        return myEntity;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
