package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.MyEntityService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.service.dto.MyEntityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing MyEntity.
 */
@RestController
@RequestMapping("/api")
public class MyEntityResource {

    private final Logger log = LoggerFactory.getLogger(MyEntityResource.class);

    private static final String ENTITY_NAME = "myEntity";

    private final MyEntityService myEntityService;

    public MyEntityResource(MyEntityService myEntityService) {
        this.myEntityService = myEntityService;
    }

    /**
     * POST  /my-entities : Create a new myEntity.
     *
     * @param myEntityDTO the myEntityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new myEntityDTO, or with status 400 (Bad Request) if the myEntity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/my-entities")
    @Timed
    public ResponseEntity<MyEntityDTO> createMyEntity(@Valid @RequestBody MyEntityDTO myEntityDTO) throws URISyntaxException {
        log.debug("REST request to save MyEntity : {}", myEntityDTO);
        if (myEntityDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new myEntity cannot already have an ID")).body(null);
        }
        MyEntityDTO result = myEntityService.save(myEntityDTO);
        return ResponseEntity.created(new URI("/api/my-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /my-entities : Updates an existing myEntity.
     *
     * @param myEntityDTO the myEntityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated myEntityDTO,
     * or with status 400 (Bad Request) if the myEntityDTO is not valid,
     * or with status 500 (Internal Server Error) if the myEntityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/my-entities")
    @Timed
    public ResponseEntity<MyEntityDTO> updateMyEntity(@Valid @RequestBody MyEntityDTO myEntityDTO) throws URISyntaxException {
        log.debug("REST request to update MyEntity : {}", myEntityDTO);
        if (myEntityDTO.getId() == null) {
            return createMyEntity(myEntityDTO);
        }
        MyEntityDTO result = myEntityService.save(myEntityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, myEntityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /my-entities : get all the myEntities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of myEntities in body
     */
    @GetMapping("/my-entities")
    @Timed
    public List<MyEntityDTO> getAllMyEntities() {
        log.debug("REST request to get all MyEntities");
        return myEntityService.findAll();
    }

    /**
     * GET  /my-entities/:id : get the "id" myEntity.
     *
     * @param id the id of the myEntityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the myEntityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/my-entities/{id}")
    @Timed
    public ResponseEntity<MyEntityDTO> getMyEntity(@PathVariable String id) {
        log.debug("REST request to get MyEntity : {}", id);
        MyEntityDTO myEntityDTO = myEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(myEntityDTO));
    }

    /**
     * DELETE  /my-entities/:id : delete the "id" myEntity.
     *
     * @param id the id of the myEntityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/my-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteMyEntity(@PathVariable String id) {
        log.debug("REST request to delete MyEntity : {}", id);
        myEntityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
