package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.MyEntityDTO;
import java.util.List;

/**
 * Service Interface for managing MyEntity.
 */
public interface MyEntityService {

    /**
     * Save a myEntity.
     *
     * @param myEntityDTO the entity to save
     * @return the persisted entity
     */
    MyEntityDTO save(MyEntityDTO myEntityDTO);

    /**
     *  Get all the myEntities.
     *
     *  @return the list of entities
     */
    List<MyEntityDTO> findAll();

    /**
     *  Get the "id" myEntity.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    MyEntityDTO findOne(String id);

    /**
     *  Delete the "id" myEntity.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
}
