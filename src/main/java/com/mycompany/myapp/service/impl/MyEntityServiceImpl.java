package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.MyEntityService;
import com.mycompany.myapp.domain.MyEntity;
import com.mycompany.myapp.repository.MyEntityRepository;
import com.mycompany.myapp.service.dto.MyEntityDTO;
import com.mycompany.myapp.service.mapper.MyEntityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing MyEntity.
 */
@Service
public class MyEntityServiceImpl implements MyEntityService{

    private final Logger log = LoggerFactory.getLogger(MyEntityServiceImpl.class);

    private final MyEntityRepository myEntityRepository;

    private final MyEntityMapper myEntityMapper;

    public MyEntityServiceImpl(MyEntityRepository myEntityRepository, MyEntityMapper myEntityMapper) {
        this.myEntityRepository = myEntityRepository;
        this.myEntityMapper = myEntityMapper;
    }

    /**
     * Save a myEntity.
     *
     * @param myEntityDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MyEntityDTO save(MyEntityDTO myEntityDTO) {
        log.debug("Request to save MyEntity : {}", myEntityDTO);
        MyEntity myEntity = myEntityMapper.toEntity(myEntityDTO);
        myEntity = myEntityRepository.save(myEntity);
        return myEntityMapper.toDto(myEntity);
    }

    /**
     *  Get all the myEntities.
     *
     *  @return the list of entities
     */
    @Override
    public List<MyEntityDTO> findAll() {
        log.debug("Request to get all MyEntities");
        return myEntityRepository.findAll().stream()
            .map(myEntityMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one myEntity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public MyEntityDTO findOne(String id) {
        log.debug("Request to get MyEntity : {}", id);
        MyEntity myEntity = myEntityRepository.findOne(UUID.fromString(id));
        return myEntityMapper.toDto(myEntity);
    }

    /**
     *  Delete the  myEntity by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete MyEntity : {}", id);
        myEntityRepository.delete(UUID.fromString(id));
    }
}
