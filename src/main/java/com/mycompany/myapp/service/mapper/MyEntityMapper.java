package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.MyEntityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MyEntity and its DTO MyEntityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MyEntityMapper extends EntityMapper <MyEntityDTO, MyEntity> {
    
    

}
