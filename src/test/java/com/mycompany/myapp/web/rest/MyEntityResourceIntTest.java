package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.AbstractCassandraTest;
import com.mycompany.myapp.JhipsterNewApp;

import com.mycompany.myapp.domain.MyEntity;
import com.mycompany.myapp.repository.MyEntityRepository;
import com.mycompany.myapp.service.MyEntityService;
import com.mycompany.myapp.service.dto.MyEntityDTO;
import com.mycompany.myapp.service.mapper.MyEntityMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MyEntityResource REST controller.
 *
 * @see MyEntityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JhipsterNewApp.class)
public class MyEntityResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PHONE = 1;
    private static final Integer UPDATED_PHONE = 2;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private MyEntityRepository myEntityRepository;

    @Autowired
    private MyEntityMapper myEntityMapper;

    @Autowired
    private MyEntityService myEntityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restMyEntityMockMvc;

    private MyEntity myEntity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MyEntityResource myEntityResource = new MyEntityResource(myEntityService);
        this.restMyEntityMockMvc = MockMvcBuilders.standaloneSetup(myEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MyEntity createEntity() {
        MyEntity myEntity = new MyEntity()
            .name(DEFAULT_NAME)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL);
        return myEntity;
    }

    @Before
    public void initTest() {
        myEntityRepository.deleteAll();
        myEntity = createEntity();
    }

    @Test
    public void createMyEntity() throws Exception {
        int databaseSizeBeforeCreate = myEntityRepository.findAll().size();

        // Create the MyEntity
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(myEntity);
        restMyEntityMockMvc.perform(post("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the MyEntity in the database
        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeCreate + 1);
        MyEntity testMyEntity = myEntityList.get(myEntityList.size() - 1);
        assertThat(testMyEntity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMyEntity.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testMyEntity.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    public void createMyEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = myEntityRepository.findAll().size();

        // Create the MyEntity with an existing ID
        myEntity.setId(UUID.randomUUID());
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(myEntity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMyEntityMockMvc.perform(post("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = myEntityRepository.findAll().size();
        // set the field null
        myEntity.setName(null);

        // Create the MyEntity, which fails.
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(myEntity);

        restMyEntityMockMvc.perform(post("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isBadRequest());

        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = myEntityRepository.findAll().size();
        // set the field null
        myEntity.setPhone(null);

        // Create the MyEntity, which fails.
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(myEntity);

        restMyEntityMockMvc.perform(post("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isBadRequest());

        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllMyEntities() throws Exception {
        // Initialize the database
        myEntityRepository.save(myEntity);

        // Get all the myEntityList
        restMyEntityMockMvc.perform(get("/api/my-entities"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myEntity.getId().toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }

    @Test
    public void getMyEntity() throws Exception {
        // Initialize the database
        myEntityRepository.save(myEntity);

        // Get the myEntity
        restMyEntityMockMvc.perform(get("/api/my-entities/{id}", myEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(myEntity.getId().toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    public void getNonExistingMyEntity() throws Exception {
        // Get the myEntity
        restMyEntityMockMvc.perform(get("/api/my-entities/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateMyEntity() throws Exception {
        // Initialize the database
        myEntityRepository.save(myEntity);
        int databaseSizeBeforeUpdate = myEntityRepository.findAll().size();

        // Update the myEntity
        MyEntity updatedMyEntity = myEntityRepository.findOne(myEntity.getId());
        updatedMyEntity
            .name(UPDATED_NAME)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL);
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(updatedMyEntity);

        restMyEntityMockMvc.perform(put("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isOk());

        // Validate the MyEntity in the database
        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeUpdate);
        MyEntity testMyEntity = myEntityList.get(myEntityList.size() - 1);
        assertThat(testMyEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMyEntity.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testMyEntity.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    public void updateNonExistingMyEntity() throws Exception {
        int databaseSizeBeforeUpdate = myEntityRepository.findAll().size();

        // Create the MyEntity
        MyEntityDTO myEntityDTO = myEntityMapper.toDto(myEntity);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMyEntityMockMvc.perform(put("/api/my-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(myEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the MyEntity in the database
        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteMyEntity() throws Exception {
        // Initialize the database
        myEntityRepository.save(myEntity);
        int databaseSizeBeforeDelete = myEntityRepository.findAll().size();

        // Get the myEntity
        restMyEntityMockMvc.perform(delete("/api/my-entities/{id}", myEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MyEntity> myEntityList = myEntityRepository.findAll();
        assertThat(myEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MyEntity.class);
        MyEntity myEntity1 = new MyEntity();
        myEntity1.setId(UUID.randomUUID());
        MyEntity myEntity2 = new MyEntity();
        myEntity2.setId(myEntity1.getId());
        assertThat(myEntity1).isEqualTo(myEntity2);
        myEntity2.setId(UUID.randomUUID());
        assertThat(myEntity1).isNotEqualTo(myEntity2);
        myEntity1.setId(null);
        assertThat(myEntity1).isNotEqualTo(myEntity2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MyEntityDTO.class);
        MyEntityDTO myEntityDTO1 = new MyEntityDTO();
        myEntityDTO1.setId(UUID.randomUUID());
        MyEntityDTO myEntityDTO2 = new MyEntityDTO();
        assertThat(myEntityDTO1).isNotEqualTo(myEntityDTO2);
        myEntityDTO2.setId(myEntityDTO1.getId());
        assertThat(myEntityDTO1).isEqualTo(myEntityDTO2);
        myEntityDTO2.setId(UUID.randomUUID());
        assertThat(myEntityDTO1).isNotEqualTo(myEntityDTO2);
        myEntityDTO1.setId(null);
        assertThat(myEntityDTO1).isNotEqualTo(myEntityDTO2);
    }
}
